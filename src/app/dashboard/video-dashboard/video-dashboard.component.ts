import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, share, switchMap } from 'rxjs/operators';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css'],
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(vds: VideoDataService, route: ActivatedRoute) {
    // use if you need to subscribe twice but only want 1 instance of work
    // this.videoList = vds.loadVideos().pipe(share());
    this.videoList = vds.loadVideos();

    this.selectedVideo = route.queryParamMap.pipe(
      map((params) => params.get('selectedVideo')),
      switchMap((videoId) => vds.fetchVideo(videoId))
    );
  }
}
