import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css'],
})
export class StatFiltersComponent {
  filterForm: FormGroup;

  constructor(fb: FormBuilder) {
    this.filterForm = fb.group({
      title: ['', Validators.required],
    });
  }
}
