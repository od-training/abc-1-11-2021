import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const apiUrl = 'https://api.angularbootcamp.com';

export interface ViewDetail {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  id: string;
  author: string;
  title: string;
  viewDetails: ViewDetail[];
}

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(apiUrl + '/videos');
  }

  fetchVideo(id: string): Observable<Video> {
    return this.http.get<Video>(apiUrl + '/videos/' + id);
  }
}
